import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('http://circus.qamania.org/login/')

WebUI.setText(findTestObject('Object Repository/Page_Circus/input_Error text_username'), 'test')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_Circus/input_Error text_password'), 'P9ET2sDE0SE=')

WebUI.click(findTestObject('Page_Circus/buttonLogin'))

WebUI.click(findTestObject('Page_Circus/a_user info'))

WebUI.verifyElementAttributeValue(findTestObject('Page_Circus/input_Name_formInput oneMoreInput'), 'value', username, 0)

WebUI.closeBrowser()

